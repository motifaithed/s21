console.log("Hello World");
// 3.
let users = ["Iron Man", "Captain America", "Hulk", "Thor"];
console.log("Before add:");
console.log(users);

function addUser(user) {
  users.push(user);
}

addUser("Hawk Eye");
console.log("After add:");
console.log(users);

// 4.
function getUser(index) {
  return users[index];
}

let itemFound = getUser(2);
console.log("Item or Hero found is " + itemFound);

// 5.
function deleteLastUser() {
  let lastUser = users[users.length - 1];
  users.pop();
  return lastUser;
}

const lastUser = deleteLastUser();
console.log("the last user before delete was " + lastUser);

//6.

function updateSuperHero(index, hero) {
  users[index] = hero;
}

updateSuperHero(2, "Gagamboy");
console.log(users);

//7.

function deleteUsers() {
  users = [];
}

deleteUsers();
console.log(users);

// 8.

function checkEmptyArray() {
  if (users.length > 0) {
    return false;
  } else {
    return true;
  }
}

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);
